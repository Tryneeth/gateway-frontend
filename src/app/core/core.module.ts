import {NgModule} from '@angular/core';
import {DeviceComponent} from './components/device/device.component';
import {GatewayComponent} from './components/gateway/gateway.component';
import {SharedModule} from '../shared/shared.module';
import {CoreRoutingModule} from './core-routing.module';
import {GatewayService} from './services/gateway.service';
import {DeviceService} from './services/device.service';
import {GatewayDetailsComponent} from './components/gateway/gateway-details/gateway-details.component';
import {NewDeviceComponent} from './components/device/new-device/new-device.component';
import {NewGatewayComponent} from './components/gateway/new-gateway/new-gateway.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    GatewayComponent,
    DeviceComponent,
    GatewayDetailsComponent,
    NewDeviceComponent,
    NewGatewayComponent,
  ],
  imports: [
    SharedModule,
    CoreRoutingModule,
    ReactiveFormsModule
  ],
  exports: [
  ],
  entryComponents: [GatewayDetailsComponent, NewGatewayComponent, NewDeviceComponent],
  providers: [GatewayService, DeviceService]
})
export class CoreModule {
}
