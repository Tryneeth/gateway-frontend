import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BaseHttpService} from './services/base-http.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MaterialModule} from './modules/material.module';
import {ErrorInterceptor} from './interceptor/error-interceptor';
import {ErrorNotificationComponent} from './components/error-notification/error-notification.component';

@NgModule({
  declarations: [ErrorNotificationComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [BaseHttpService, {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}],
  exports: [
    CommonModule,
    HttpClientModule,
    MaterialModule,
    ErrorNotificationComponent
  ]
})
export class SharedModule {
}
