import {Component, Input, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {DeviceService} from '../../services/device.service';
import {Device} from '../../models/device';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnDestroy {
  @Input()
  devices: Device[];
  displayedColumns: string[] = ['uid', 'vendor', 'createdDate', 'status', 'actions'];

  subscriptionsManager: Subscription = new Subscription();

  constructor(private deviceService: DeviceService) {}

  deleteDevice(device: Device) {
    this.subscriptionsManager
        .add(this.deviceService
                 .deleteByParent(device.gatewayId, device._id)
                 .subscribe((deleted) => {
                   this.devices = this.devices.filter(device => device._id !== deleted._id)
                 }));
  }

  ngOnDestroy(): void {
    this.subscriptionsManager.unsubscribe();
  }
}
