import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../shared/services/base-http.service';
import {Gateway} from '../models/gateway';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class GatewayService extends BaseHttpService<Gateway> {

  constructor(protected http: HttpClient) {
    super(http, 'gateways');
  }
}
