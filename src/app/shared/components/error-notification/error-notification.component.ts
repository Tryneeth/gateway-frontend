import { Component, OnInit } from '@angular/core';
import {ErrorHandlingService} from '../../services/error-handling.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-error-notification',
  templateUrl: './error-notification.component.html',
  styleUrls: ['./error-notification.component.scss']
})
export class ErrorNotificationComponent implements OnInit {
  error: string;
  constructor(private errorService: ErrorHandlingService, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.errorService.notifier.subscribe((errorMessage) => {
      if (errorMessage)
      this.snackBar.open(errorMessage, 'OK', {})
    });
  }

}
