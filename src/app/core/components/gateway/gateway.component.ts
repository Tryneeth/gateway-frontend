import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import {GatewayService} from '../../services/gateway.service';
import {Subscription} from 'rxjs';
import {Gateway} from '../../models/gateway';
import {MatDialog, MatSnackBar} from '@angular/material';
import {NewGatewayComponent} from './new-gateway/new-gateway.component';
import {NewDeviceComponent} from '../device/new-device/new-device.component';
import {DeviceService} from '../../services/device.service';
import {Device} from '../../models/device';
import {GatewayDetailsComponent} from './gateway-details/gateway-details.component';

@Component({
  selector: 'app-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.scss']
})
export class GatewayComponent implements AfterViewInit, OnDestroy {
  gateways: Gateway[];
  displayedColumns: string[] = ['serial', 'name', 'ipv4', 'devices', 'actions'];

  subscriptionsManager: Subscription = new Subscription();

  constructor(
    private gatewayService: GatewayService,
    private deviceService: DeviceService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar) {
  }

  ngAfterViewInit() {
    this.loadGateways();
  }

  details(gateway: Gateway) {
    let devices: Device[];
    this.deviceService.findAllByParent(gateway._id)
        .subscribe(devicesFromGateway => {
          devices = devicesFromGateway;
          this.dialog.open(GatewayDetailsComponent,
            {width: '550px', data: {gateway: gateway, devices: devices}})
              .afterClosed()
              .subscribe(() => this.loadGateways());
        });

  }

  addGateway() {
    this.subscriptionsManager
        .add(this.dialog
                 .open(NewGatewayComponent)
                 .afterClosed()
                 .subscribe((result) => {
                     console.log(result);
                     if (result !== undefined)
                       this.subscriptionsManager
                           .add(this.gatewayService
                                    .save({...result})
                                    .subscribe(
                                      (_) => this.loadGateways()
                                    ))
                   }
                 ));
  }

  loadGateways() {
    this.subscriptionsManager
        .add(this.gatewayService.findAll()
                 .subscribe((data) => {
                   this.gateways = data;
                 }));
  }

  deleteGateway(gateway: Gateway) {
    this.subscriptionsManager
        .add(this.gatewayService
                 .delete(gateway._id)
                 .subscribe((deleted) => {
                   this.gateways = this.gateways.filter(gateway => gateway._id !== deleted._id)
                 }));
  }

  addDevice(gateway: Gateway) {
    if (gateway.devicesCount === 10) {
      this.snackBar.open('This gateway reached maximum devices allowed (10).', 'OK');
    } else {
      this.subscriptionsManager
          .add(this.dialog
                   .open(NewDeviceComponent, {width: '250px', data: gateway})
                   .afterClosed()
                   .subscribe((result) => {
                     if (result)
                       this.deviceService.saveByParent(gateway._id, result)
                           .subscribe(() =>
                             this.loadGateways()
                           )
                   }));
    }
  }

  ngOnDestroy(): void {
    this.subscriptionsManager.unsubscribe();
  }

}
