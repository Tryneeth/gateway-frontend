import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-new-gateway',
  templateUrl: './new-gateway.component.html',
  styleUrls: ['./new-gateway.component.scss']
})
export class NewGatewayComponent implements OnInit {
  gatewayForm: FormGroup;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<NewGatewayComponent>) {
  }

  ngOnInit() {
    const ipPattern =
      '^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$';
    this.gatewayForm = this.fb.group({
      serial: ['', Validators.required],
      name: ['', [Validators.required, Validators.pattern('^$|^[A-Za-z0-9]+')]],
      ipv4: ['', [Validators.required, Validators.pattern(ipPattern)]]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
