import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable()
export class BaseHttpService<T> {
  endpoint: string;
  resourceName: string;
  parentResource: string;

  constructor(protected http: HttpClient, resource: string, parentResource?: string) {
    this.endpoint = environment.apiUrl;
    this.resourceName = resource;
    this.parentResource = parentResource;
  }

  findAll(): Observable<T[]> {
    return this.http.get(this.endpoint + this.resourceName) as Observable<T[]>;
  }

  find(id: string): Observable<T> {
    return this.http.get(`${this.endpoint + this.resourceName}/${id}`) as Observable<T>;
  }

  save(resource: T): Observable<T> {
    return this.http.post(this.endpoint + this.resourceName, {resource}) as Observable<T>;
  }

  delete(id: string): Observable<T> {
    return this.http.delete(`${this.endpoint + this.resourceName}/${id}`) as Observable<T>;
  }

  findByParent(parentResourceId: string, id: string): Observable<T> {
    return this.http.get(`${this.parentUrl(parentResourceId)}/${this.resourceName}/${id}`) as Observable<T>;
  }

  findAllByParent(parentResourceId: string): Observable<T[]> {
    return this.http.get(`${this.parentUrl(parentResourceId)}/${this.resourceName}`) as Observable<T[]>;
  }

  saveByParent(parentResourceId: string, resource: T): Observable<T> {
    return this.http.post(`${this.parentUrl(parentResourceId)}/${this.resourceName}`, {resource}) as Observable<T>;
  }

  deleteByParent(parentResourceId: string, id: string): Observable<T> {
    return this.http.delete(`${this.parentUrl(parentResourceId)}/${this.resourceName}/${id}`) as Observable<T>;
  }

  parentUrl(parentResourceId: string): string {
    return `${this.endpoint}${this.parentResource}/${parentResourceId}`;
  }
}
