import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef, MatSlideToggleChange} from '@angular/material';
import {STATUS} from '../../../models/device';

@Component({
  selector: 'app-new-device',
  templateUrl: './new-device.component.html',
  styleUrls: ['./new-device.component.scss']
})
export class NewDeviceComponent implements OnInit {
  deviceForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<NewDeviceComponent>,
    private fb: FormBuilder) {
  }

  ngOnInit() {
    this.deviceForm = this.fb.group({
      uid: ['', [Validators.required, Validators.pattern('^$|^[0-9]+')]],
      vendor: ['', Validators.required],
      status: [STATUS.OFFLINE]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onChangeStatus($event: MatSlideToggleChange) {
    this.status.setValue($event.checked ? STATUS.ONLINE : STATUS.OFFLINE);
  }

  get status() {
    return this.deviceForm.get('status');
  }
}
