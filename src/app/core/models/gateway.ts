export class Gateway {
  _id: string;
  serial: string;
  name: string;
  ipv4: string;
  devicesCount: number;
}
