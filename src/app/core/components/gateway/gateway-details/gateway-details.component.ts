import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {Gateway} from '../../../models/gateway';
import {Device} from '../../../models/device';

@Component({
  selector: 'app-gateway-details',
  templateUrl: './gateway-details.component.html',
  styleUrls: ['./gateway-details.component.scss']
})
export class GatewayDetailsComponent{
  gateway: Gateway;
  devices: Device[];

  constructor(@Inject(MAT_DIALOG_DATA) public data) {
    this.gateway = data.gateway;
    this.devices = data.devices;
  }
}
