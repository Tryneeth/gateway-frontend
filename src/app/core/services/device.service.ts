import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../shared/services/base-http.service';
import {Device} from '../models/device';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class DeviceService extends BaseHttpService<Device> {

  constructor(protected http: HttpClient) {
    super(http, 'devices', 'gateways');
  }
}
