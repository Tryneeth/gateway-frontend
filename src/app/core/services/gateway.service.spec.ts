import {TestBed} from '@angular/core/testing';

import {GatewayService} from './gateway.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {of} from 'rxjs';
import {Gateway} from '../models/gateway';

describe('GatewayService', () => {
  let service: GatewayService;

  beforeEach(() => TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GatewayService]
    })
                          .compileComponents()
  );

  beforeEach(() => {
    service = TestBed.get(GatewayService);
  });

  it('should be created', () => {
    expect(service)
      .toBeTruthy();
  });

  it('should resolve all gateways', () => {
    spyOn(service, 'findAll').and.returnValue(of([]));

    service.findAll().subscribe((gateways) => {
      expect(gateways).not.toBeNull();
    });
  });

  it('should resolve a single gateway', () => {
    spyOn(service, 'find').and.returnValue(of(Gateway));

    service.find('id').subscribe((gateway) => {
      expect(gateway).not.toBeNull();
    });

  });

  it('should save a gateway', () => {
    spyOn(service, 'save').and.returnValue(of(Gateway));

    service.find('id').subscribe((gateway) => {
      expect(gateway).not.toBeNull();
    });

  });

  it('should delete a gateway', () => {
    spyOn(service, 'delete').and.returnValue((of(Gateway)));

    service.delete('id').subscribe((gateway) => {
      expect(gateway).not.toBeNull();
    });
  });

});
