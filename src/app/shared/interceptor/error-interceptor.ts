import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import { get } from 'lodash';
import {ErrorHandlingService} from '../services/error-handling.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private errorService: ErrorHandlingService
  ) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    const request = req.clone({ headers });
    return next.handle(request).pipe(
      catchError((errorResponse: HttpErrorResponse) => {
        console.log(errorResponse);
        this.errorService.notifyError(errorResponse.error.message);
        return throwError(errorResponse);
      }),
    );
  }
}
