export class Device {
  _id: string;
  uid: number;
  createdDate: Date;
  status: STATUS;
  gatewayId: string;
}

export enum STATUS {
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE'
}
