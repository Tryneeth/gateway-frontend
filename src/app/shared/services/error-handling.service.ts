import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService {

  constructor() { }

  notifier: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  notifyError(message: string): void{
    this.notifier.next(message);
  }

}
